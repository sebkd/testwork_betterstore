"""Модуль моделей"""

from datetime import datetime
from typing import Union

from odmantic import Model


class Item(Model):
    key: Union[str, None] = None
    value: dict
    timestamp: float = datetime.timestamp(datetime.now())
