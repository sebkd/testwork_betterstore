"""
Основной модуль
запускать python main.py --reload
также необходимо запустить docker c mongoDB - указано в README
"""

from typing import List

from fastapi import FastAPI, HTTPException

from odmantic import AIOEngine

from model.model import Item

app = FastAPI()

engine = AIOEngine()


@app.post("/save/{key}", response_model=Item, response_model_include={"key", "value", "timestamp"})
async def create_item(key: str, item: Item):
    """
    Функция записи экземпляра модели
    :param key: ключ
    :param item: модель, включающая обязательный атрибут value
    :return: модель с разрешенными полями
    """
    item.key = key
    await engine.save(item)
    return item


@app.get("/show/{key}", response_model=Item, response_model_include={"value", "timestamp"})
async def get_item_by_key(key: str):
    """
    Функция выбора экземпляра модели по ключу
    :param key: ключ
    :return: модель с разрешенными полями
    """
    item = await engine.find_one(Item, Item.key == key)
    if item is None:
        raise HTTPException(404)
    return item


@app.delete("/del/{key}", response_model=Item, response_model_include={"value", "timestamp"})
async def delete_item_by_key(key: str):
    """
    Функция удаления экземпляра модели
    :param key: ключ
    :return:
    """
    item = await engine.find_one(Item, Item.key == key)
    if item is None:
        raise HTTPException(404)
    await engine.delete(item)
    return item


@app.get("/show_all/", response_model=List[Item])
async def get_items():
    """
    Функция выводв всех экземпляров модели со всеми полями
    :return: список экземпляров
    """
    items = await engine.find(Item)
    return items
