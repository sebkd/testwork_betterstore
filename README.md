# testwork_betterstore
Разработать на python простой web-сервер, обрабатывающий три вида запросов:
POST-запрос /save/KEY , принимающий значение VALUE и сохраняющий его (KEY - строка без пробелов, VALUE - JSON)
GET-запрос /show/KEY, возвращающий JSON следующего вида:
{"value": сохранённое значение для ключа KEY или null, если ключа нет, 
"timestamp": unix-time записи ключа (т.е. запроса /save/KEY)
DELELE-запрос /del/KEY удаляющий пару ключ-значение

необходимо запустить контейнер mongoDB
docker run --rm -p 27017:27017 mongo

